from django.contrib import admin
from .models import CustomUser
from .forms import CustomUserChangeForm, CustomUserCreationForm
from django.contrib.auth.admin import UserAdmin


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ("username", "email", "trial_duration", "trial_expires")
    fieldsets = UserAdmin.fieldsets + (
        ("Extra Fields", {"fields": ("trial_expires",)}),
    )
    # add a field in the admin to see the trial_expires


admin.site.register(CustomUser, CustomUserAdmin)
