from django.urls import path
from . import views

# from django_registration.backends.one_step.views import RegistrationView
# from .forms import CustomRegistrationForm

urlpatterns = [
    path("", views.home, name="home"),
    path("accounts/login/", views.login_user, name="login"),
    path("accounts/logout/", views.logout_user, name="logout"),
    path(
        "accounts/register/",
        views.CustomRegistrationView.as_view(),
        name="django_registration_register",
    ),
]
