from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from datetime import timedelta

# Create your models here.


class CustomUser(AbstractUser):
    """
    Added extra fields to the default user model to ease tracking of the date for the trial to expire. the trial will be activated after customer subscribe for the trial.
    """

    trial_duration = models.IntegerField(default=7)
    trial_expires = models.DateTimeField(null=True, blank=True)
