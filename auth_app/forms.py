from django_registration.forms import RegistrationFormUniqueEmail
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm

User = get_user_model()


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = UserChangeForm.Meta.fields


class CustomRegistrationForm(RegistrationFormUniqueEmail):
    class Meta(RegistrationFormUniqueEmail.Meta):
        model = User
        fields = ("username", "email", "password1", "password2")

    def __init__(self, *args, **kwargs):
        # self.request = kwargs.pop('request', None)
        super(CustomRegistrationForm, self).__init__(*args, **kwargs)
        self.fields["username"].widget.attrs["placeholder"] = "username..."
        self.fields["username"].widget.attrs["class"] = "form-control"

        self.fields["email"].widget.attrs["placeholder"] = "E-mail..."
        self.fields["email"].widget.attrs["class"] = "form-control"

        self.fields["password1"].widget.attrs["placeholder"] = "your password..."
        self.fields["password1"].widget.attrs["class"] = "form-control"

        self.fields["password2"].widget.attrs["placeholder"] = "your password again..."
        self.fields["password2"].widget.attrs["class"] = "form-control"


class CustomLoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ("username", "password")

    def __init__(self, *args, **kwargs):
        # self.request = kwargs.pop('request', None)
        super(CustomLoginForm, self).__init__(*args, **kwargs)

        self.fields["username"].widget.attrs["placeholder"] = "your username..."
        self.fields["username"].widget.attrs["class"] = "form-control"

        self.fields["password"].widget.attrs["placeholder"] = "your password..."
        self.fields["password"].widget.attrs["class"] = "form-control"
