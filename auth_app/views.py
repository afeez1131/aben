from django.shortcuts import render, redirect
from .forms import CustomRegistrationForm, CustomLoginForm
from django_registration.views import RegistrationView
from django.urls import reverse_lazy
from django.contrib import messages
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout
from django_registration import signals
from core.models import Subscription


def home(request):
    return render(request, "home.html", {})


User = get_user_model()
# declare the global variable user


class CustomRegistrationView(RegistrationView):
    """
    registration view from django-registation
    """

    form_class = CustomRegistrationForm
    success_url = reverse_lazy("login")
    template_name = "django_registration/registration_form.html"

    def register(self, form):
        new_user = form.save()
        # save the user
        new_user = authenticate(
            **{
                User.USERNAME_FIELD: new_user.get_username(),
                "password": form.cleaned_data["password1"],
            }
        )
        login(self.request, new_user)
        # log in the user
        signals.user_registered.send(
            sender=self.__class__, user=new_user, request=self.request
        )
        return new_user


def login_user(request):
    """
    handle the login, using a custom form declared in the forms.py

    """
    if request.user.is_authenticated:
        # if a user is authenticated, and try to access the login page, redirect them to the homepage
        return redirect("home")

    else:
        if request.method == "POST":
            form = CustomLoginForm(request.POST, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                next = request.GET.get("next", None)
                # get the next parameter from the post request

                user = authenticate(request, username=username, password=password)
                # authenticate the username and password
                # authenticate the user
                if user is not None:
                    # if the user exist
                    sub_inst = Subscription.objects.filter(user=user)
                    if sub_inst.exists() and user.trial_expires != None:
                        if user.trial_expires < timezone.now():
                            """
                            check if the time for the trial expires is less than the current time. if Yes, change the status of trial to False"""
                            user.subscription.trial = False
                            user.save()
                    else:

                        login(request, user)
                        if next is None:
                            return redirect("home")
                        else:
                            return redirect(next)
            else:
                # if the form has errors, or is invalid
                return render(request, "account/login.html", {"form": form})

        else:
            form = CustomLoginForm()
        return render(request, "account/login.html", {"form": form})


def logout_user(request):
    if request.user.is_authenticated and request.method == "POST":
        # check to see if user is logged in, and the request method is Post
        logout(request)
        return redirect("home")
        # redirect to the home page

    return render(request, "account/logout.html", {})
