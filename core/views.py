from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth.decorators import login_required
import stripe
from .models import Subscription
from django.contrib import messages
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from datetime import timedelta


stripe.api_key = settings.STRIPE_SECRET_KEY
subs_product_price = settings.STRIPE_PRODUCT_ID
publish_key = settings.STRIPE_PUBLISHABLE_KEY


@login_required()
def subscribe(request):
    amount = request.user.subscription.amount
    if request.method == "POST":
        return redirect("charge")
    return render(request, "subscription.html", {"amount": amount})


@login_required()
def charge(request):
    """
    handles the payment using the form provided by stripe js.
    after getting the user details,
    - create an account for the user on stripe
    - subscribe for the plan using the customer instance, trial_period, and the price for the plan.
    """
    if request.method == "POST":

        try:
            user = request.user
            email = user.email  # get the email from the current user instance
            name = user.username  # get username
            # get the token sent from the post request by stripe
            source = request.POST["stripeToken"]
            # hardcode the description to be trial subcription
            description = "trial subscription"

            """
            create the stripe customer
            """
            customer = stripe.Customer.create(
                email=email, name=name, source=source, description=description
            )

            """
            subscribe for the customer using the customer instance,
            trial duration and the price of product to subscribe for.
            since i created the product on stripe already, i will make use of the product id
            gotten from stripe.
            """

            subscription = stripe.Subscription.create(
                customer=customer,
                trial_period_days=user.trial_duration,
                items=[
                    {"price": subs_product_price},
                ],
            )

            user.trial_expires = timezone.now() + timedelta(days=user.trial_duration)
            user.save()

            local_sub = Subscription.objects.get(user=request.user)
            local_sub.subscription_id = subscription["id"]
            local_sub.trial = True
            local_sub.subscribed = True
            local_sub.save()

            return redirect("subscription_successful")
        except Exception as e:
            messages.warning(request, "Warning %s" % e)
            return redirect("charge")

    return render(request, "charge.html", {})


def subscription_success(request):
    """
    users will be redirected to this page after subscription.
    """

    return render(request, "subscription_success.html", {})


@login_required()
def cancel_subscription(request):
    """This will be used by the subscribed users to cancel their subscriptions."""
    try:
        user_subs = Subscription.objects.get(
            Q(subscribed=True) | Q(trial=True), user=request.user
        )

    except ObjectDoesNotExist:
        messages.info(request, "You have no subscriptions to cancel.")
        return redirect("home")

    if request.method == "POST":
        subs_user = Subscription.objects.get(user=request.user)
        user_subs_id = subs_user.subscription_id
        print("User ;", user_subs_id)
        try:
            # customer =
            stripe.Subscription.delete(
                user_subs_id,
            )
            # s = stripe.Subscription.list(limit=3)
            subscription = Subscription.objects.get(user=request.user)
            subscription.cancel = True
            subscription.save()

            subs_user.trial = False
            subs_user.subscribed = False
            subs_user.save()

            messages.success(request, "Subscription cancelled succesfully")
            return redirect("home")
        except Exception as e:
            messages.warning(request, e)
            return redirect("cancel_subscription")
    return render(request, "cancel_subscription.html", {})
