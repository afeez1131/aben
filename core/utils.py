import stripe


def create_stripe_customer(email, name, source, description):
    customer = stripe.Customer.create(
        email=email, name=name, source=source, description=description
    )
    return customer


def create_subscription(customer, trial_period, price):
    subscription = stripe.Subscription.create(
        customer=customer,
        trial_period_days=user.trial_duration,
        items=[
            {"price": sub_id},
        ],
    )
    return subscription
