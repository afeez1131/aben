from django.urls import path
from . import views


urlpatterns = [
    path("subscribe/", views.subscribe, name="subscribe"),
    path("subscribe/charge/", views.charge, name="charge"),
    path(
        "subscribe/successful/",
        views.subscription_success,
        name="subscription_successful",
    ),
    path("cancel/subscription/", views.cancel_subscription, name="cancel_subscription"),
]
