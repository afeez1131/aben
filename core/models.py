from django.db import models
from django.conf import settings
from django.db.models.signals import post_save


class Subscription(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=49.99)
    trial = models.BooleanField(default=False)
    subscription_id = models.CharField(max_length=40, blank=True, null=True)
    subscribed = models.BooleanField(default=False)
    cancel = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


def post_save_turn_subscribe_on(sender, instance, created, *args, **kwargs):
    if created:
        Subscription.objects.create(user=instance)


post_save.connect(post_save_turn_subscribe_on, sender=settings.AUTH_USER_MODEL)
